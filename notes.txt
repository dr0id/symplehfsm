



use cases
=========

1. if the statemachine is in a state, it is considered that it is also in all super states
2. all states and substates share the same interface (lukimov subst principle)
3. program by difference: a sub state only needs to implement the difference to its superstate
   -> if a event can not be handled by a state, then it should be handled by its superstate
4. entry and and exit events (like a constructor/destructor in OOP) should be processed in 
   right order, e.g.: entry in order from outer states to inner states, exit in the reverse order
5. a state should be exchangable with a different implementation (-> no inheritance to implement
   state hirarchy)
6. safe event handler methods in a state, e.g.: after changing state it shold not be possible to 
   execute more code in that method (-> return next state or transition)

motivation
==========

there are many different ways to implement statemachines:
- switch case (not so elegant and gets messy fast)
- state pattern (simple, does not help with state hirarchy)
- state transition tables (how does this work with hirarchical statemachines??)
- dynamic tree like structures
   
   
example
=======

Hirarchical statemachines allow for elegant design, with superstates it is possible
to share logic between states (substates).

    +---------------------------------------------------------------------------+
    |                                    A                                      |
    +---------------------------------------------------------------------------+
    |                                                                           |
    |   +-------------------+   +-------------------+   +-------------------+   |
    |   |        B          |   |        C          |   |        D          |   |
    |   +-------------------+   +-------------------+   +-------------------+   |
    |   |                   |   |                   |   |                   |   |
    |   | +---+ +---+ +---+ |   |                   |   | +-----+  +-----+  |   |
    |   | | E | | F | | G | |   |                   |   | |  H  |  |  I  |  |   |
    |   | +---+ +---+ +---+ |   |                   |   | +-----+  +-----+  |   |
    |   | |   | |   | |   | |   |                   |   | |     |  |     |  |   |
    |   | +---+ +---+ +---+ |   |                   |   | +-----+  +-----+  |   |
    |   |                   |   |                   |   |                   |   |
    |   +-------------------+   +-------------------+   +-------------------+   |
    |                                                                           |
    +---------------------------------------------------------------------------+    

   
                A
                |
        +-------+-------+
        |       |       |
        B       C       D
        |               |
    +---+---+       +---+---+
    |   |   |       |       |
    E   F   G       H       I

    Transitions:
        F -> H:     F.exit; B.exit, D.entry, H.entry
        E -> F:     E.exit; F.entry
        G -> C:     G.exit; B.exit; C.entry
        etc.
        
        
        B -> C: either  E.exit; B.exit; C.entry
                or      F.exit; B.exit; C.entry
                or      G.exit; B.exit; C.entry
        but dont implement all
        
        
hirachical statemachines can always be flattened (but get unwieldy if number of states grow):

    +-----+     +-----+     +-----+     +----+      +-----+     +-----+
    | ABE |     | ABF |     | ABG |     | AC |      | ADH |     | ADI |
    +-----+     +-----+     +-----+     +----+      +-----+     +-----+
    |     |     |     |     |     |     |    |      |     |     |     |
    |     |     |     |     |     |     |    |      |     |     |     |
    +-----+     +-----+     +-----+     +----+      +-----+     +-----+

    F -> H converts to ABF -> ADH:      ABF.exit; ADH.entry
    etc.
    
    B -> C converts to three transitions:   ABE -> AC
                                            ABF -> AC
                                            ABG -> AC


Transitiontable (does not help with hirarchy or use multiple levels of transitiontables?):                                            
        
    +---------+--------+--------+--------+--------+
    |\ events |        |        |        |        |
    | \______ | event1 | event2 | event3 | event4 |
    | state  \|        |        |        |        |
    +---------+--------+--------+--------+--------+
    |  state1 | state2 |        |        |        |
    +---------+--------+--------+--------+--------+
    |  state2 |        | state1 |        | state3 |
    +---------+--------+--------+--------+--------+
    |  state3 | state2 | state1 |        |        |
    +---------+--------+--------+--------+--------+

    
    
    
    
                A
                |
        +-------+-------+
        |       |       |
        B       C       D
        |               |
    +---+---+       +---+---+
    |   |   |       |       |
    E   F   G       H       I

    
http://homepage.usask.ca/~ctl271/810/lowest_common_ancestor.pdf    
    

    
















