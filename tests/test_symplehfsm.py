#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
TODO: modul doc string
"""

__author__ = u"dr0iddr0id {at} gmail [dot] com (C) 2010"

import operator

import symplehfsm
from symplehfsm import BaseState
from symplehfsm import Transition
from symplehfsm import BaseHFSMTests
from symplehfsm import Structure
from symplehfsm import SympleHFSM
from symplehfsm import DummyLogger


import unittest





silent_logger = symplehfsm.default_logger
silent_logger = DummyLogger()

# -----------------------------------------------------------------------------
# TODO: loop action back to event -> queue?
# -----------------------------------------------------------------------------

# making a statemachine testable based on: http://accu.org/index.php/journals/1548


# -----------------------------------------------------------------------------
#
# Statechart used to test the SympleHFSM
# based on
# [Samek] Miro Samek, Practical Statecharts in C/C++, CMP Books 2002.
# There's a companion website with additional information: http://www.quantum-leaps.com
# taken from: http://accu.org/index.php/journals/252
#
# see also: http://en.wikipedia.org/wiki/UML_state_machine#Local_versus_external_transitions
# making a statemachine testable based on: http://accu.org/index.php/journals/1548






# -----------------------------------------------------------------------------
class BaseStateTests(unittest.TestCase):

    def setUp(self):
        self.state = BaseState()

    def test_name_is_set(self):
        name = "XXX"
        state = BaseState(name=name)

        self.assertTrue(name in str(state), "name not set")

    def test_parent_set_through_constructor(self):

        self.state.name = "parend"
        child = BaseState("child", self.state)

        self.assertTrue(child.parent == self.state, "wrong parent set to child")
        self.assertTrue(child in self.state.children, "child not registered in parent")

    def test_self_is_not_child(self):
        self.assertTrue(self.state.is_child(self.state) == False, "the state itself should not be a child")

    def test_direct_child(self):
        child = BaseState()
        self.state.add(child)
        self.assertTrue(child.is_child(self.state), "child should be a child")

    def test_multiple_children(self):

        child1 = BaseState()
        child2 = BaseState()
        child3 = BaseState()
        self.state.add(child1)
        child1.add(child2)
        child2.add(child3)

        self.assertTrue(child1.is_child(self.state), "child1 should be a child")
        self.assertTrue(child2.is_child(self.state), "child2 should be a child")
        self.assertTrue(child3.is_child(self.state), "child3 should be a child")
        self.assertTrue(child2.is_child(child1), "child2 should be a child")
        self.assertTrue(child3.is_child(child1), "child3 should be a child")
        self.assertTrue(child3.is_child(child2), "child3 should be a child")

    def test_representation_is_string(self):
        repr = str(self.state)
        self.assertTrue(len(repr) > 0, "should not be empty")
        self.assertTrue(isinstance(repr, type("")), "should be a string")

    def test_parent_is_not_child(self):
        child = BaseState()
        self.assertTrue(self.state.is_child(child) == False, "parent should not be a child")

    def test_self_hast_not_self_as_child(self):
        self.assertTrue(self.state.has_child(self.state) == False, "the state itself should not be a child")

    def test_has_direct_child(self):
        child = BaseState()
        self.state.add(child)
        self.assertTrue(self.state.has_child(child), "child should be a child")

    def test_has_multiple_children(self):
        child1 = BaseState()
        child2 = BaseState()
        child3 = BaseState()
        self.state.add(child1)
        child1.add(child2)
        child2.add(child3)

        self.assertTrue(self.state.has_child(child1), "child1 should be a child")
        self.assertTrue(self.state.has_child(child2), "child2 should be a child")
        self.assertTrue(self.state.has_child(child3), "child3 should be a child")
        self.assertTrue(child1.has_child(child2), "child2 should be a child")
        self.assertTrue(child1.has_child(child3), "child3 should be a child")
        self.assertTrue(child2.has_child(child3), "child3 should be a child")

    def test_child_has_not_parent_as_child(self):
        child = BaseState()
        self.assertTrue(child.has_child(self.state) == False, "parent should not be a child")

    def test_add_child(self):
        child = BaseState()

        self.state.add(child)
        self.assertTrue(child.parent == self.state, "child has wrong parent")
        self.assertTrue(self.state.children[0] == child, "parent state should have child")

    def test_add_child_and_initial(self):
        child = BaseState()

        self.state.add(child, True)
        self.assertTrue(child.parent == self.state, "child has wrong parent")
        self.assertTrue(self.state.children[0] == child, "parent state should have child")
        self.assertTrue(self.state.initial == child, "initial state not set")

    def test_add_child_initial_only_once(self):
        child = BaseState()
        child2 = BaseState()

        self.state.add(child, True)
        try:
            self.state.add(child2, True)
            self.fail("should throw exception when trying to add another state as inital")
        except BaseState.InitialStateAlreadySetError:
            pass
        except Exception as ex:
            self.fail("not the expected exception: " + str(ex))

    def test_add_returns_child(self):
        child = BaseState()
        ret = self.state.add(child)
        self.assertTrue(ret==child, "add should return added child")

    def test_check_if_child_has_parent(self):
        child = BaseState()
        child.parent = 1
        try:
            self.state.add(child)
            self.fail("should have thrown exception because child has a parent set already")
        except BaseState.ParentAlreadySetError:
            pass
        except Exception as ex:
            self.fail("not the expected exception: " + str(ex))

    def test_remove_child(self):
        child = BaseState()
        self.state.add(child)

        self.state.remove(child)

        self.assertTrue(child.parent is None, "parent of removed child still set")
        try:
            self.state.children.index(child)
            self.fail("child should not be in children after remove")
        except ValueError:
            pass
        except Exception as e:
            self.fail(str(e))

    def test_remove_child_set_as_initial(self):
        child = BaseState()
        child2 = BaseState()
        self.state.add(child, True)
        self.state.add(child2)

        self.state.remove(child, child2)

        self.assertTrue(child.parent is None, "parent of removed child still set")
        self.assertTrue(self.state.initial == child2, "initial not reset to other child")
        try:
            self.state.children.index(child)
            self.fail("child should not be in children after remove")
        except ValueError:
            pass
        except Exception as ex:
            self.fail("not the expected exception: " + str(ex))

    def test_remove_child_set_as_initial_exception(self):
        child = BaseState()
        self.state.add(child, True)

        try:
            self.state.remove(child)
            self.fail("removing initial state without replacement should raise a Exception")
        except BaseState.InitialNotReplacedError as e:
            pass
        except Exception as ex:
            self.fail("not the expected exception: " + str(ex))

    def test_remove_child_set_as_initial_replacment_is_child(self):
        child = BaseState()
        child2 = BaseState()
        self.state.add(child, True)

        try:
            self.state.remove(child, child2)
            self.fail("should raise exception because replacement child is not a child of the state")
        except BaseState.ReplacementStateIsNotChildError:
            pass
        except Exception as ex:
            self.fail("not the expected exception: " + str(ex))

    def test_check_consistency_check_initial_not_set(self):
        child = BaseState()
        child2 = BaseState()

        self.state.add(child)
        self.state.add(child2)
        try:
            self.state.check_consistency()
            self.fail("should throw initial missing exception")
        except BaseState.InitialNotSetError:
            pass
        except Exception as ex:
            self.fail("not the expected exception: " + str(ex))

    def test_check_consistency_check_initial(self):
        child = BaseState()
        child2 = BaseState()

        self.state.add(child)
        self.state.add(child2, True)
        try:
            self.state.check_consistency()
        except Exception as e:
            self.fail(str(e))

    def test_check_consistency_check_initial_wrong_parent(self):
        child = BaseState()
        child2 = BaseState()

        self.state.add(child)
        self.state.add(child2, True)
        child.parent = child2
        try:
            self.state.check_consistency()
            self.fail("should raise WrongParentError")
        except BaseState.WrongParentError as e:
            pass
        except Exception as ex:
            self.fail("not the expected exception: " + str(ex))




# -----------------------------------------------------------------------------

class SympleHFSMTransitionTests(unittest.TestCase):

    def setUp(self):
            # def __init__(self, target_state, action=None, guard=None, name=None):
        self.target = "target"
        self.action = "action"
        self.guard = "guard"
        self.name = "name"
        self.else_target = "else target"
        self.else_action = "else action"
        self.transition = Transition(self.target, self.action, self.guard, \
                                        self.else_target, self.else_action, self.name)

    def test_attributes(self):
        self.assertTrue(self.transition.target == self.target)
        self.assertTrue(self.transition.action == self.action)
        self.assertTrue(self.transition.guard == self.guard)
        self.assertTrue(self.transition.else_target == self.else_target)
        self.assertTrue(self.transition.else_action == self.else_action)
        self.assertTrue(self.transition.name == self.name)

    def test_representation_is_string(self):
        repr = str(self.transition)
        self.assertTrue(len(repr) > 0, "should not be empty")
        self.assertTrue(isinstance(repr, type("")), "should be a string")

# -----------------------------------------------------------------------------

class StructureTests(unittest.TestCase):
    """
    ..todo: test _get_methodcalls() !!!!!

    Tests the Structure class.
    """

    def setUp(self):
        self.name = "asdlfkjdflkjUIPOIU"
        self.structure = Structure(self.name, logger=silent_logger)

        self.state1 = "ARBD"
        self.state2 = 234234234
        self.state3 = object()

    def test_that_name_is_set(self):
        self.assertTrue(self.structure.name == self.name, "name not set!")

    def test_add_root(self):
        #     def add_state(self, state, parent, initial, entry_action=None, exit_action=None):
        self.structure.add_state(self.state1, None, None)

        self.assertTrue(self.structure.root == self.state1, "no or wrong root set")
        self.assertTrue(len(self.structure.states) == 1, "only one state (root) should be present")

    def test_adding_second_root_raises_exception(self):
        self.structure.add_state(self.state1, None, None)

        try:
            self.structure.add_state(self.state2, None, None)
            self.fail("should have raised a RootAlreadySetOrParentMissingError exception")
        except Structure.RootAlreadySetOrParentMissingError as e:
            pass
        except Exception as ex:
            self.fail("not the expected exception: " + str(ex))

    def test_adding_state_twice_raises_exception(self):
        self.structure.add_state(self.state1, None, None)
        self.structure.add_state(self.state3, self.state1, None)
        try:
            self.structure.add_state(self.state3, self.state1, None)
            self.fail("should have raised StateIdentifierAlreadyUsed exception")
        except Structure.StateIdentifierAlreadyUsed as e:
            pass
        except Exception as ex:
            self.fail("not the expected exception: " + str(ex))

    def test_add_state(self):
        self.structure.add_state(self.state1, None, None)
        self.structure.add_state(self.state2, self.state1, None)

        parent = self.structure.states[self.state1]
        child = self.structure.states[self.state2]

        self.assertTrue(parent.has_child(child), "child not added")
        self.assertTrue(child.parent == parent, "wrong parent set to child")

    def test_add_state_parent_unkown(self):
        try:
            self.structure.add_state(self.state1, "unkown parent identifier", self.state2)
            self.fail("should have raised 'ParentUnkownError'")
        except self.structure.ParentUnkownError:
            pass
        except Exception as ex:
            self.fail("not the expected exception: " + str(ex))

    def test_add_transition_to_unkown_state(self):

        try:
            # def add_trans(self, state, event, target, action=None, guard=None, name=None):
            self.structure.add_trans("aaaa", "event1", self.state2)
            self.fail("should have raised StateUnknownError")
        except symplehfsm.StateUnknownError as e:
            pass
        except Exception as ex:
            self.fail("not the expected exception: " + str(ex))

    def test_add_transition_unkown_target(self):
        self.structure.add_state(self.state1, None, None)

        try:
            self.structure.add_trans(self.state1, "event", "unkown_state_identifier")
            self.fail("should have raised StateUnknownError")
        except symplehfsm.StateUnknownError as e:
            pass
        except Exception as ex:
            self.fail("not the expected exception: " + str(ex))

    def test_add_transition_target_not_set(self):
        self.structure.add_state(self.state1, None, None)

        try:
            self.structure.add_trans(self.state1, "event", "unknown state")
            self.fail("should have raised StateUnknownError")
        except symplehfsm.StateUnknownError as e:
            pass
        except Exception as ex:
            self.fail("not the expected exception: " + str(ex))
            
    def test_add_transition_else_target_only_possible_with_guard(self):
        self.structure.add_state(self.state1, None, None)
        try:
            self.structure.add_transition(self.state1, "event", self.state1, else_target=self.state1)
            self.fail("should have raised GuardMissingForElseTransitionError")
        except symplehfsm.GuardMissingForElseTransitionError as e:
            pass
        except Exception as ex:
            self.fail("not the expected exception: " + str(ex))
        
    def test_add_transition_else_action_only_possible_with_guard(self):
        self.structure.add_state(self.state1, None, None)
        try:
            self.structure.add_transition(self.state1, "event", self.state1, else_action=1)
            self.fail("should have raised GuardMissingForElseTransitionError")
        except symplehfsm.GuardMissingForElseTransitionError as e:
            pass
        except Exception as ex:
            self.fail("not the expected exception: " + str(ex))
            
    def test_add_transition_else_target_raises_StateUnknownError_if_not_known(self):
        self.structure.add_state(self.state1, None, None)

        try:
            self.structure.add_transition(self.state1, "event", self.state1, "action", "guard", "unkown_target")
            self.fail("should have raised StateUnknownError")
        except symplehfsm.StateUnknownError as e:
            pass
        except Exception as ex:
            self.fail("not the expected exception: " + str(ex))
        
    def test_representation_is_string(self):
        repr = str(self.structure)
        self.assertTrue(len(repr) > 0, "should not be empty")
        self.assertTrue(isinstance(repr, type("")), "should be a string")

    def test_adding_transition_same_event_twice_on_same_state_raises_error(self):
        self.structure.add_state(self.state1, None, None)
        event_id = 12341234
        self.structure.add_trans(self.state1, event_id, None)
        try:
            self.structure.add_trans(self.state1, event_id, None)
            self.fail("should have raised EventAlreadyDefinedError")
        except Structure.EventAlreadyDefinedError as e:
            pass
        except Exception as ex:
            self.fail("not the expected exception: " + str(ex))

    def test_adding_transitions(self):
        event_id = 12341234
        event_id1 = 123412334
        event_id2 = 1234

        self.structure.add_state(self.state1, None, None)
        self.structure.add_state(self.state2, self.state1, None)
        self.structure.add_state(self.state3, self.state2, None)

        self.structure.add_trans(self.state1, event_id, None)
        self.structure.add_trans(self.state1, event_id1, None)
        self.structure.add_trans(self.state1, event_id2, None)
        self.structure.add_trans(self.state2, event_id, None)
        self.structure.add_trans(self.state3, event_id, None)

    def test_check_consistency_check_initial_not_set(self):
        self.structure.add_state(self.state1, None, None) # root
        self.structure.add_state(self.state2, self.state1, None) # child 1
        self.structure.add_state(self.state3, self.state1, None) # child 2

        try:
            self.structure.check_consistency()
            self.fail("should throw initial missing exception")
        except BaseState.InitialNotSetError:
            pass
        except Exception as ex:
            self.fail("not the expected exception: " + str(ex))

    def test_check_consistency_check_initial(self):
        self.structure.add_state(self.state1, None, None) # root
        self.structure.add_state(self.state2, self.state1, None) # child 1
        self.structure.add_state(self.state3, self.state1, True) # child 2
        # child = BaseState()
        # child2 = BaseState()

        # self.state.add(child)
        # self.state.add(child2, True)
        try:
            self.structure.check_consistency()
        except Exception as e:
            self.fail(str(e))

    def test_check_consistency_check_initial_wrong_parent(self):
        self.structure.add_state(self.state1, None, None) # root
        self.structure.add_state(self.state2, self.state1, True) # child 1
        self.structure.add_state(self.state3, self.state1, None) # child 2
        self.structure.states[self.state3].parent = self.state2
        # child = BaseState()
        # child2 = BaseState()

        # self.state.add(child)
        # self.state.add(child2, True)
        # child.parent = child2
        try:
            self.structure.check_consistency()
            self.fail("should raise WrongParentError")
        except BaseState.WrongParentError as e:
            pass
        except Exception as ex:
            self.fail("not the expected exception: " + str(ex))



# -----------------------------------------------------------------------------

class StructureOptimizationTests(unittest.TestCase):

    def setUp(self):
        self.structure = Structure("optimized structure", logger=silent_logger)
        self.structure.add_state("root", None, None)
        self.structure.add_state("level1-1", "root", True)
        self.structure.add_state("level1-2", "root", None)
        self.structure.add_state("level1-3", "root", None)

        self.structure.add_state("level2-1-1", "level1-1", True)
        self.structure.add_state("level2-1-2", "level1-1", None)
        self.structure.add_state("level2-1-3", "level1-1", False)

        self.structure.add_state("level2-2-1", "level1-2", True)
        self.structure.add_state("level2-2-2", "level1-2", False)
        self.structure.add_state("level2-2-3", "level1-2", False)

        self.structure.add_state("level2-3-1", "level1-2", False)
        self.structure.add_state("level2-3-2", "level1-2", False)
        self.structure.add_state("level2-3-3", "level1-2", False)

        self.structure.add_state("level3-1-1-1", "level2-1-1", False)
        self.structure.add_state("level3-1-1-2", "level2-1-1", None)
        self.structure.add_state("level3-1-1-3", "level2-1-1", False)

        self.structure.add_state("level3-1-2-1", "level2-1-2", True)
        self.structure.add_state("level3-1-2-2", "level2-1-2", None)
        self.structure.add_state("level3-1-2-3", "level2-1-2", False)

        self.structure.add_state("level3-2-1-1", "level2-2-1", False)
        self.structure.add_state("level3-2-1-2", "level2-2-1", None)
        self.structure.add_state("level3-2-1-3", "level2-2-1", False)

        self.structure.add_state("level3-3-2-1", "level2-3-2", False)
        self.structure.add_state("level3-3-2-2", "level2-3-2", None)
        self.structure.add_state("level3-3-2-3", "level2-3-2", False)

    def test_do_optimization_twice(self):
        self.structure.do_optimize()
        self.assertTrue(self.structure.is_optimized, "structure should indicate that it is optimized")
        self.structure._get_methodcallers = self._get_methodcallers_testable
        self.structure.do_optimize()

    def _get_methodcallers_testable(self, *args, **kwargs):
        self.fail("should not call '_get_methodcallers' on second 'do_optimization'")

    def test_do_optimization_states(self):
        try:
            self.structure.do_optimize()
        except Exception as e:
            self.fail(str(e))

    def test_do_optimization_with_transitions(self):
        #                   handling state,   event, next state,           action,                    guard
        self.structure.add_trans("level1-1", "a", "level2-2-1")
        self.structure.add_trans("level1-2", "b", "level2-3-1")
        self.structure.add_trans("level2-1-1", "c", "level3-3-2-1")
        try:
            self.structure.do_optimize()
        except Exception as e:
            self.fail(str(e))

# -----------------------------------------------------------------------------
import operator
from operator import methodcaller

class SympleHFSMTests(unittest.TestCase):

    def setUp(self):
        self.state_root_name = "root"
        self.sub_state1 = "sub1"
        self.sub_state2 = "sub2"
        self.root_entry_name = "root_entry"
        self.init_state_entry_name = "init_state_entry"
        self.root_exit_name = "root_exit"
        self.state_exit_name = "state_exit"
        self.structure = Structure("SympleHFSMTests.structure", logger=silent_logger)
        self.structure.add_state(self.state_root_name, None, None, methodcaller(self.root_entry_name), methodcaller(self.root_exit_name))
        self.structure.add_state(self.sub_state1, self.state_root_name, False)
        self.structure.add_state(self.sub_state2, self.state_root_name, True, methodcaller(self.init_state_entry_name), methodcaller(self.state_exit_name))

        self.event_id_loop_event = "this is the loop event trigger"
        #     def add_trans(self, state, event, target, action=None, guard=None, name=None):
        self.structure.add_trans(self.sub_state2, self.event_id_loop_event, self.sub_state1, self._action_calling_event)

        # def add_trans(self, state, event, target, action=None, guard=None, name=None):
        self.event_id_guard_true = "guard true"
        self.structure.add_trans(self.sub_state2, self.event_id_guard_true, self.sub_state1, self._record_action, self._guard_true)
        self.event_id_guard_false = "guard false"
        self.structure.add_trans(self.sub_state2, self.event_id_guard_false, self.sub_state1, self._record_action, self._guard_false)

        self.actions = BaseHFSMTests.RecordingActions()
        self.machine = SympleHFSM(self.structure, self.actions, "SympleHFSMTests.machine", logger=silent_logger)

    def test_repr(self):
        msg = "" + str(self.machine)
        self.assertTrue(len(msg) > 0, "no name set")

    def test_set_state_unkown_state(self):
        try:
            self.machine.set_state("222")
            self.fail("should have raised StateUnknownError")
        except symplehfsm.StateUnknownError as e:
            pass
        except Exception as ex:
            self.fail("not the expected exception: " + str(ex))

    def test_set_state_currently_handling_event(self):
        self.machine._currently_handling_event = True
        try:
            self.machine.set_state(self.state_root_name)
            self.fail("should have raised ReentrantEventException")
        except SympleHFSM.ReentrantEventException as e:
            pass
        except Exception as ex:
            self.fail("not the expected exception: " + str(ex))

    def test_set_state_normal(self):
        self.machine.set_state(self.state_root_name)
        self.assertTrue(self.machine.current_state == self.state_root_name, "wrong state set after set_state")

    def test_init_optimizedstructure(self):
        self.structure.do_optimize()
        self._do_test_init(False)
        self.assertTrue(self.machine.handle_event == self.machine._handle_event_normal)

    def test_init_unoptimizedstructure(self):
        self._do_test_init(False)
        self.assertTrue(self.machine.handle_event == self.machine._handle_event_normal)

    def test_init_use_optimization_unoptimizedstructure(self):
        self._do_test_init(True)
        self.assertTrue(self.machine.handle_event == self.machine._handle_event_normal)

    def _do_test_init(self, use_optimization):
        self.assertTrue(self.machine.current_state is None, "state should not be set yet")
        try:
            self.machine.init(use_optimization)
        except Exception as e:
            self.fail("error: " + str(e))
        self.assertTrue(self.machine.current_state is not None, "current state should be set to a state")
        self.assertTrue(self.machine.current_state == self.sub_state2, "wrong initial state")

    def test_init_calls_entries(self):
        self.machine.init()
        self.assertTrue([self.root_entry_name, self.init_state_entry_name] == self.actions.captured_actions,
                        "inits not called on states: " + str(self.actions.captured_actions))

    def test_exit_calls_exits(self):
        self.machine.init()
        self.actions.captured_actions = []
        self.machine.exit()

        self.assertTrue([self.state_exit_name, self.root_exit_name] == self.actions.captured_actions,
                        "exits not called on states: " + str(self.actions.captured_actions))

    def test_handling_event_not_initialized(self):
        try:
            self.machine.handle_event("")
            self.fail("should have thrown exception")
        except SympleHFSM.NotInitializedException as e:
            pass
        except Exception as ex:
            self.fail("not the expected exception: " + str(ex))

    def test_handling_unkonw_event(self):
        self.handle_unkown_event()

    def handle_unkown_event(self):
        self.machine.init(True)
        current_state = self.machine.current_state
        self.machine.handle_event("e")
        self.assertTrue(self.machine.current_state == current_state, "current state should be same as before, expected: {0}  actual: {1}".format(current_state, self.machine.current_state))

    def test_handle_event_during_action(self):
        self.machine.init(False)
        self._handle_event_during_action()

    def _action_calling_event(self, actions):
        self.machine.handle_event("error")

    def _handle_event_during_action(self):
        try:
            self.machine.handle_event(self.event_id_loop_event)
            self.fail("should have raised a ReentrantEventException")
        except SympleHFSM.ReentrantEventException as e:
            pass
        except Exception as ex:
            self.fail("not the expected exception: " + str(ex))

    def _record_action(self, actions):
        actions._record_action()

    def _guard_true(self, actions):
        self.actions._guard_true()
        return True

    def _guard_false(self, actions):
        self.actions._guard_false()
        return False

    def test_guard_false(self):
        self.machine.init()
        self.machine.handle_event(self.event_id_guard_false)
        self.assertTrue(self._guard_false.__name__ in self.actions.captured_actions, "guard false not called")

    def test_guard_true(self):
        self.machine.init()
        self.machine.handle_event(self.event_id_guard_true)
        self.assertTrue(self._guard_true.__name__ in self.actions.captured_actions, "guard true not called")

    def test_calling_handle_event_without_init_raises_exception(self):
        try:
            self.machine.handle_event(self.event_id_guard_true)
            self.fail("should have raised NotInitializedException")
        except SympleHFSM.NotInitializedException as ex:
            pass
        except Exception as ex:
            self.fail("not the expected exception: " + str(ex))

    def test_calling_init_multifple_times_raises_error(self):
        self.machine.init()
        try:
            self.machine.init()
            self.fail("should raise InitAlreadyCalledError")
        except SympleHFSM.InitAlreadyCalledError as ex:
            pass
        except Exception as ex:
            self.fail("not the expected exception: " + str(ex))

    def test_calling_exit_multiple_times_does_not_matter(self):
        self.machine.init()
        self.machine.exit()
        self.machine.exit()
        
    def test_calling_init_after_exit_should_initialize_normally(self):
        self.machine.init()
        self.machine.exit()
        self.machine.init()


# -----------------------------------------------------------------------------

class SympleHFSMTestsOptimized(SympleHFSMTests):

    def setUp(self):
        SympleHFSMTests.setUp(self)
        self.structure.do_optimize()
        self.machine = SympleHFSM(self.structure, self.actions, logger=silent_logger) #, "SympleHFSMTests.machine")

    def test_handle_event_during_action(self):
        self.structure.do_optimize()
        self.machine.init(True)
        self._handle_event_during_action()

    def test_init_use_optimization_unoptimizedstructure(self):
        # this test can only be performed in a not optimized structure
        pass

    def test_init_use_optimization_optimizedstructure(self):
        self.structure.do_optimize()
        self._do_test_init(True)
        self.assertTrue(self.machine.handle_event == self.machine._handle_event_optimized)






# -----------------------------------------------------------------------------


#    *                                                                                                         
#    |                                                                                                         
#  init                                                                                                         
#    |  +-----------------------------------------------------------------------------------------------------+
#    |  |                                                s0                                                   |
#    |  +-----------------------------------------------------------------------------------------------------+
#    |  | entry/                                                                                              |
#    +->| exit/                                                                                               |
#       | i/action_i                                                                                          |
#       |                                                                                                     |
#       |       +--------------------------------+    +--------------------------------------------------+    |
#       |   *-->|        s1                      |    |               s2                                 |    |
#       |       +--------------------------------+    +--------------------------------------------------+    |
#       |       | entry/                         |-c->| entry/              +--------h[!foo]/ foo=1---+  |    |
#       |<--d---| exit/                          |    | exit/               |                         |  |    |
#       |       | j/action_j                     |    | k/action_k          |                         |  |    |
#       |       |                                |    |                     |                         |  |    |
#       |       |                                |    |                     |                         |  |    |
#       |       |     +---------------+          |<-c-|      +------------------------------------+   |  |    |
#       |       | *-->|      s11      |          |    |  *-->|             s21                    |<--+  |    |
#       |       |     +---------------+          |    |      +------------------------------------+      |    |
#       |    +--|     | entry/        |          |    |      | entry/                             |      |    |
#       |   a|  |     | exit/         |<----------f---|      | exit/                              |      |    |
#       |    |  |     | n/action_n    |          |    |      | l[foo]/action_l                    |      |    |
#       |    +->|     | h[foo]/ foo=0;|          |    |      |                                    |      |    |
#       |       |     |               |          |    |      |                                    |      |    |
#       |       |     |               |          |    |      |       +----------------------+     |      |    |
#       |       |     |               |          |    |      |   *-->|     s211             |     |      |    |
#       |       |--b->|               |          |    |      |       +----------------------+     |      |    |
#       |       |     |               |          |--------f--------->| entry/               |     |      |    |
#       |       |     |               |          |    |      |       | exit/                |--------g------->|
#       |       |     |               |          |    |      |       | m/action_m           |     |      |    |
#       |       |     |               |-----------------g----------->| o[foo]/act_o--X->    |     |      |    |
#       |       |     |               |          |    |      |       |               |      |     |      |    |
#       |       |     |               |          |    |      |--b--->|           act_o_el   |<-------e--------|
#       |       |     |               |          |    |      |       |               |      |     |      |    |
#       |       |     |               |          |    |      |<---d--|               +----------->|      |    |
#       |       |     |               |          |    |      |       |                      |     |      |    |
#       |       |  +->|               |          |    |      |       +----------------------+     |      |    |
#       |       |  |  |               |          |    |      |                                    |      |    |
#       |       |  |  +---------------+          |    |      +------------------------------------+      |    |
#       |       |  |                |            |    |                      |                           |    |
#       |       |  |                |            |    |                      |                           |<-+ |
#       |       |  +-o[foo]/act_o---X---act_o_else--->|                      |                           |  | |
#       |       |                                |    |                      |                           |  | |
#       |       |                                |    |<---p[foo]/action_p---X---------action_p_else--------+ |
#       |       |                                |    |                                                  |    |
#       |       |                                |    |                                                  |    |
#       |       +--------------------------------+    +--------------------------------------------------+    |
#       |                                                                                                     |
#       +-----------------------------------------------------------------------------------------------------+
#                                                                     |
#                                                                   exit
#                                                                     |
#                                                                     +-->O
#
#
#
#
#      As far as I understand it, current_state always points to either s11 or s211 (one of the leaf states).
#      Also for the transitions triggered by an event I assume it works as follows:
#
#       event|    from -> to      | transition actions                                                  | transition type
#       -----------------------------------------------------------------------------------------------------------------
#        init:      s0 ->  s11:     s0.entry, s1.entry, s11.entry                                         
#        exit:    s211 ->   s0:     s211.exit, s21.exit, s2.exit, s0.exit                                             
#                  s11 ->   s0:     s11.exit, s1.exit, s0.exit                                            external transition
#           a:      s1 ->   s1:     s11.exit,  s1.exit,  s1.entry, s11.entry                              external transition
#           b:      s1 ->  s11:     s11.exit, s11.entry                                                   local transition
#                  s21 -> s211:     s211.exit, s211.entry                                                 local transition
#           c:      s1 ->   s2:     s11.exit,  s1.exit,  s2.entry, s21.entry, s211.entry                  external transition
#                   s2 ->   s1:     s211.exit, s21.exit,  s2.exit, s1.entry, s11.entry                    external transition
#           d:      s1 ->   s0:     s11.exit,  s1.exit,  s1.entry, s11.entry                              local transition
#                 s211 ->  s21:     s211.exit, s211.entry                                                 local transition
#           e:      s0 -> s211:     s11.exit,  s1.exit, s2.entry, s21.entry, s211.entry                   local transition
#                   s0 -> s211:     s211.exit, s21.exit,  s2.exit, s2.entry, s21.entry, s211.entry        local transition
#           f:      s2 ->  s11:     s211.exit, s21.exit,  s2.exit, s1.entry, s11.entry                    external transition
#                   s1 -> s211:     s11.exit,  s1.exit,  s2.entry, s21.entry, s211.entry                  external transition
#           g:     s11 -> s211:     s11.exit,  s1.exit,  s2.entry, s21.entry, s211.entry                  external transition
#                 s211 ->   s0:     s211.exit, s21.exit,  s2.exit,  s1.entry, s11.entry                   external transition
#           h:     s11 ->  s11:     foo==True: actions.unset_foo                                          internal transition, guard true
#                  s11 ->  s11:     foo==False: do nothing                                                internal transition, guard false
#                  s21 ->  s21:     foo==False: s211.exit, s21.exit, actions.set_foo, s21.entry, s211.entry  external transition, guard true
#                  s21 ->  s21:     foo==True: do nothing                                                 external transition, guard false
#           i:     s11 ->  s11:     action_i                                                              internal transition
#                 s211 -> s211:     action_i                                                              internal transition
#           j:     s11 ->  s11:     action_j                                                              internal transition
#           k:    s211 -> s211:     action_k                                                              internal transition
#           l:    s211 -> s211:     foo==True: action_l                                                   internal transition
#                 s211 -> s211:     foo==False: do nothing                                                internal transition
#           m:    s211 -> s211:     action_m                                                              internal transition
#           n:     s11 ->  s11:     action_n                                                              internal transition
#           o:     s11 ->  s11:     foo=True: s11.exit, act_o, s11.entry                                  external transition
#                  s11 ->   s2:     foo=False: s11.exit, s1.exit, act_o_el, s2.entry, s21.entry, s211.entry  external transition (else path of guard)
#                 s211 -> s211:     foo=True: act_o                                                       internal transition
#                 s211 -> s211:     foo=False: s211.exit, act_o_el, s211.entry                            local transition (else path of guard)
#           p:    s211 -> s211:     foo=True: s211.exit, s21.exit, action_p, s21.entry, s211.entry        local transition
#                 s211 -> s211:     foo=False: s211.exit, s21.exit, s2.exit, action_p_else, s2.entry, s21.entry, s211.entry     external transition (else path of guard)


class Actions(object):
    """
    The Actions the statemachine can execute.
    """
    def set_foo(self): raise NotImplementedError()
    def unset_foo(self): raise NotImplementedError()
    def check_foo(self): raise NotImplementedError("check_foo needs to be overridden to return bool")
    def check_foo_inverted(self): raise NotImplementedError("check_foo_inverted needs to be overridden to return !check_foo")
    # following are just to prove it works correctly
    def enter_s0(self): raise NotImplementedError()
    def exit_s0(self): raise NotImplementedError()
    def enter_s1(self): raise NotImplementedError()
    def exit_s1(self): raise NotImplementedError()
    def enter_s11(self): raise NotImplementedError()
    def exit_s11(self): raise NotImplementedError()
    def enter_s2(self): raise NotImplementedError()
    def exit_s2(self): raise NotImplementedError()
    def enter_s21(self): raise NotImplementedError()
    def exit_s21(self): raise NotImplementedError()
    def enter_s211(self): raise NotImplementedError()
    def exit_s211(self): raise NotImplementedError()
    def trans_s1_to_s1_a(self): raise NotImplementedError()
    def trans_s1_to_s11_b(self): raise NotImplementedError()
    def trans_s21_to_s211_b(self): raise NotImplementedError()
    def trans_s1_to_s2_c(self): raise NotImplementedError()
    def trans_s2_to_s1_c(self): raise NotImplementedError()
    def trans_s1_to_s0_d(self): raise NotImplementedError()
    def trans_s211_to_s21_d(self): raise NotImplementedError()
    def trans_s0_to_s211_e(self): raise NotImplementedError()
    def trans_s1_to_s211_f(self): raise NotImplementedError()
    def trans_s2_to_s11_f(self): raise NotImplementedError()
    def trans_s11_to_s211_g(self): raise NotImplementedError()
    def trans_s211_to_s0_g(self): raise NotImplementedError()
    def trans_s11_to_s11_h(self): raise NotImplementedError()
    def trans_s21_to_s21_h(self): raise NotImplementedError()
    def action_i(self): raise NotImplementedError()
    def action_j(self): raise NotImplementedError()
    def action_k(self): raise NotImplementedError()
    def action_l(self): raise NotImplementedError()
    def action_m(self): raise NotImplementedError()
    def action_n(self): raise NotImplementedError()
    def action_o(self): raise NotImplementedError()
    def action_o_else(self): raise NotImplementedError()
    def action_p(self): raise NotImplementedError()
    def action_p_else(self): raise NotImplementedError()
    
# ------------------------------------------------------------------------------

# ------------------------------------------------------------------------------

class EventEnum(object):
    """
    Event identifiers of the statemachine (the events it can handle).
    """
    # just make sure that the values are unique!
    a = 0
    b = 1
    c = 2
    d = 3
    e = 4
    f = 5
    g = 6
    h = 7
    i = 8
    j = 9
    k = 10
    l = 11
    m = 12
    n = 13
    o = 14
    p = 15

# ------------------------------------------------------------------------------
# REQUIREMENTS:
    # - events interface
    # - actions interface (usage using different action interface implementations for test purposes)
    # - one state declaration/instanciation for each sm-type (sharing states as static datastructure since state itself are stateless)
    # - hierarchical states
    # - automatic calling entry/exits in a transition
    # - testable


# PROS: 
    # - simpler to setup
    # - less error prone
    # - less line of code to write
# CONS:
    # - identifiers are strings (not necesairly), maybe 'enums'
    # - events are identifiers ('enums')
    # - typesafety
    
from operator import methodcaller

class MyStateMachine(object):

    sm_structure = Structure(logger=silent_logger)
    
    #                         state,    parent, initial,            entry,        exit
    sm_structure.add_state("s0",          None, False,   methodcaller(Actions.enter_s0.__name__), methodcaller(Actions.exit_s0.__name__))
    sm_structure.add_state(  "s1",        "s0",  True,   methodcaller("enter_s1"), methodcaller("exit_s1"))
    sm_structure.add_state(    "s11",     "s1",  True,  methodcaller("enter_s11"), methodcaller("exit_s11"))
    sm_structure.add_state(  "s2",        "s0", False,   methodcaller("enter_s2"), methodcaller("exit_s2"))
    sm_structure.add_state(    "s21",     "s2",  True,  methodcaller("enter_s21"), methodcaller("exit_s21"))
    sm_structure.add_state(      "s211", "s21",  True, methodcaller("enter_s211"), methodcaller("exit_s211"))
    
    #                handling state,   event, next state,           action,                    guard
    sm_structure.add_trans("s0",         "e",    "s211",  methodcaller(Actions.trans_s0_to_s211_e.__name__), None) # local transition
    sm_structure.add_trans(  "s1",       "a",      "s1",    methodcaller("trans_s1_to_s1_a"), None) # local transition
    sm_structure.add_trans(  "s1",       "b",     "s11",   methodcaller("trans_s1_to_s11_b"), None) # local transition
    sm_structure.add_trans(  "s1",       "c",      "s2",    methodcaller("trans_s1_to_s2_c"), None) # external transition
    sm_structure.add_trans(  "s1",       "d",      "s0",    methodcaller("trans_s1_to_s0_d"), None) # local transition
    sm_structure.add_trans(  "s1",       "f",    "s211",  methodcaller("trans_s1_to_s211_f"), None) # external transition
    sm_structure.add_trans(    "s11",    "g",    "s211", methodcaller("trans_s11_to_s211_g"), None) # external transition
    sm_structure.add_trans(    "s11",    "h",      None,  methodcaller("trans_s11_to_s11_h"), methodcaller("check_foo")) # internal transition with guard
    sm_structure.add_trans(  "s2",       "c",      "s1",    methodcaller("trans_s2_to_s1_c"), None) # external transition
    sm_structure.add_trans(  "s2",       "f",     "s11",   methodcaller("trans_s2_to_s11_f"), None) # external transition
    sm_structure.add_trans(    "s21",    "b",    "s211", methodcaller("trans_s21_to_s211_b"), None) # local transition
    sm_structure.add_trans(    "s21",    "h",     "s21",  methodcaller("trans_s21_to_s21_h"), methodcaller("check_foo_inverted"), "s211-h")
    sm_structure.add_trans(      "s211", "d",     "s21", methodcaller("trans_s211_to_s21_d"), None) # local transition
    sm_structure.add_trans(      "s211", "g",      "s0",  methodcaller("trans_s211_to_s0_g"), None) # external transition
    sm_structure.add_trans("s0",         "i",      None,  methodcaller("action_i"), None) # internal transition
    sm_structure.add_trans(  "s1",       "j",      None,  methodcaller("action_j"), None) # internal transition
    sm_structure.add_trans(  "s2",       "k",      None,  methodcaller("action_k"), None) # internal transition
    sm_structure.add_trans(    "s21",    "l",      None,  methodcaller("action_l"), methodcaller("check_foo")) # internal transition
    sm_structure.add_trans(      "s211", "m",      None,  methodcaller("action_m"), None) # internal transition
    sm_structure.add_trans(    "s11",    "n",      None,  methodcaller("action_n"), None) # internal transition
    sm_structure.add_transition(    "s11",       "o",     "s11",  methodcaller("action_o"), methodcaller("check_foo"), "s2", methodcaller("action_o_else")) # external transition / external transition
    sm_structure.add_transition(      "s211",    "o",     None,  methodcaller("action_o"), methodcaller("check_foo"), "s211", methodcaller("action_o_else")) # internaltransition / external transition
    sm_structure.add_transition(      "s2",    "p",     "s21",  methodcaller("action_p"), methodcaller("check_foo"), "s2", methodcaller("action_p_else")) # internaltransition / external transition
    sm_structure.add_transition("s0", "z", "s0", methodcaller("action_z"))
#           p:    s211 -> s211:     foo=True: s211.exit, s21.exit, action_p, s21.entry, s211.entry        local transition
#                 s211 -> s211:     foo=False: s211.exit, s21.exit, s2.exit, action_p_else, s2.entry, s21.entry, s211.entry     external transition (else path of guard)

    def __init__(self, actions):
        self.sm = SympleHFSM(self.sm_structure, actions, logger=silent_logger)

    def init(self):
        self.sm.init()
        
    def exit(self):
        self.sm.exit()

    def set_state(self, new_state):
        self.sm.set_state(new_state)
        
    def _get_current_state(self):
        return self.sm.current_state
    current_state = property(_get_current_state)
    
    def a(self):
        self.sm.handle_event("a")
    def b(self):
        self.sm.handle_event("b")
    def c(self):
        self.sm.handle_event("c")
    def d(self):
        self.sm.handle_event("d")
    def e(self):
        self.sm.handle_event("e")
    def f(self):
        self.sm.handle_event("f")
    def g(self):
        self.sm.handle_event("g")
    def h(self):
        self.sm.handle_event("h")
    def i(self):
        self.sm.handle_event("i")
    def j(self):
        self.sm.handle_event("j")
    def k(self):
        self.sm.handle_event("k")
    def l(self):
        self.sm.handle_event("l")
    def m(self):
        self.sm.handle_event("m")
    def n(self):
        self.sm.handle_event("n")
    def o(self):
        self.sm.handle_event("o")
    def p(self):
        self.sm.handle_event("p")

class SympleHFSMTestAllFeatures(BaseHFSMTests):
    """
    Testcases for MyStateMachine using the BaseHFSMTests as base.
    """
    # -- inner classes ---#
    class TActions(Actions):
        """Test Actions for testing, captures all actions for comparison"""
        class AEnum(object):
            """Define an 'enum' to have comparable values for each action"""
            # make sure each variable has a unique value!
            SETFOO = "SETFOO"
            UNSETFOO = "UNSETFOO"
            CHECKFOO = "CHECKFOO"
            ENTERS0 = "ENTERS0"
            EXITS0 = "EXITS0"
            ENTERS1 = "ENTERS1"
            EXITS1 = "EXITS1"
            ENTERS11 = "ENTERS11"
            EXITS11 = "EXITS11" 
            ENTERS2 = "ENTERS2"
            EXITS2 = "EXITS2"
            ENTERS21 = "ENTERS21"
            EXITS21 = "EXITS21"
            ENTERS211 = "ENTERS211"
            EXITS211 = "EXITS211"
            TRANS_S1_TO_S1_A = "TRANS_S1_TO_S1_A"
            TRANS_S1_TO_S11_B = "TRANS_S1_TO_S11_B"
            TRANS_S21_TO_S211_B = "TRANS_S21_TO_S211_B"
            TRANS_S1_TO_S2_C = "TRANS_S1_TO_S2_C"
            TRANS_S2_TO_S1_C = "TRANS_S2_TO_S1_C"
            TRANS_S1_TO_S0_D = "TRANS_S1_TO_S0_D"
            TRANS_S211_TO_S21_D = "TRANS_S211_TO_S21_D"
            TRANS_S0_TO_S211_E = "TRANS_S0_TO_S211_E"
            TRANS_S1_TO_S211_F = "TRANS_S1_TO_S211_F"
            TRANS_S2_TO_S11_F = "TRANS_S2_TO_S11_F"
            TRANS_S11_TO_S211_G = "TRANS_S11_TO_S211_G"
            TRANS_S211_TO_S0_G = "TRANS_S211_TO_S0_G"
            TRANS_S11_TO_S11_H = "TRANS_S11_TO_S11_H"
            TRANS_S21_TO_S21_H = "TRANS_S21_TO_S21_H"
            ACTION_I = "ACTION_I"
            ACTION_J = "ACTION_J"
            ACTION_K = "ACTION_K"
            ACTION_L = "ACTION_L"
            ACTION_M = "ACTION_M"
            ACTION_N = "ACTION_N"
            ACT_O = "ACTION_O"
            ACT_O_EL = "ACTION_O_ELSE"
            ACTION_P = "ACTION_P"
            ACTION_P_ELSE = "ACTION_P_ELSE"
        # make it read only
        AEnum.__setattr__ = None
        
        def __init__(self):
            self.captured_actions = []
            self.foo = False
        
        # actions for guarded event/transition
        def set_foo(self): self.captured_actions.append(self.AEnum.SETFOO)
        def unset_foo(self): self.captured_actions.append(self.AEnum.UNSETFOO)
        def check_foo(self):
            self.captured_actions.append(self.AEnum.CHECKFOO)
            return self.foo
        def check_foo_inverted(self):
            return not self.check_foo()
        # following are just to prove it works correctly
        def enter_s0(self): self.captured_actions.append(self.AEnum.ENTERS0)
        def exit_s0(self): self.captured_actions.append(self.AEnum.EXITS0)
        def enter_s1(self): self.captured_actions.append(self.AEnum.ENTERS1)
        def exit_s1(self): self.captured_actions.append(self.AEnum.EXITS1)
        def enter_s11(self): self.captured_actions.append(self.AEnum.ENTERS11)
        def exit_s11(self): self.captured_actions.append(self.AEnum.EXITS11)
        def enter_s2(self): self.captured_actions.append(self.AEnum.ENTERS2)
        def exit_s2(self): self.captured_actions.append(self.AEnum.EXITS2)
        def enter_s21(self): self.captured_actions.append(self.AEnum.ENTERS21)
        def exit_s21(self): self.captured_actions.append(self.AEnum.EXITS21)
        def enter_s211(self): self.captured_actions.append(self.AEnum.ENTERS211)
        def exit_s211(self): self.captured_actions.append(self.AEnum.EXITS211)
        def separator(self): self.captured_actions.append(self.AEnum._I)
        # transition acctions
        def trans_s1_to_s1_a(self): self.captured_actions.append(self.AEnum.TRANS_S1_TO_S1_A)
        def trans_s1_to_s11_b(self): self.captured_actions.append(self.AEnum.TRANS_S1_TO_S11_B)
        def trans_s21_to_s211_b(self): self.captured_actions.append(self.AEnum.TRANS_S21_TO_S211_B)
        def trans_s1_to_s2_c(self): self.captured_actions.append(self.AEnum.TRANS_S1_TO_S2_C)
        def trans_s2_to_s1_c(self): self.captured_actions.append(self.AEnum.TRANS_S2_TO_S1_C)
        def trans_s1_to_s0_d(self): self.captured_actions.append(self.AEnum.TRANS_S1_TO_S0_D)
        def trans_s211_to_s21_d(self): self.captured_actions.append(self.AEnum.TRANS_S211_TO_S21_D)
        def trans_s0_to_s211_e(self): self.captured_actions.append(self.AEnum.TRANS_S0_TO_S211_E)
        def trans_s1_to_s211_f(self): self.captured_actions.append(self.AEnum.TRANS_S1_TO_S211_F)
        def trans_s2_to_s11_f(self): self.captured_actions.append(self.AEnum.TRANS_S2_TO_S11_F)
        def trans_s11_to_s211_g(self): self.captured_actions.append(self.AEnum.TRANS_S11_TO_S211_G)
        def trans_s211_to_s0_g(self): self.captured_actions.append(self.AEnum.TRANS_S211_TO_S0_G)
        def trans_s11_to_s11_h(self):
            self.captured_actions.append(self.AEnum.TRANS_S11_TO_S11_H)
            self.unset_foo()
        def trans_s21_to_s21_h(self):
            self.captured_actions.append(self.AEnum.TRANS_S21_TO_S21_H)
            self.set_foo()
        def action_i(self): self.captured_actions.append(self.AEnum.ACTION_I)
        def action_j(self): self.captured_actions.append(self.AEnum.ACTION_J)
        def action_k(self): self.captured_actions.append(self.AEnum.ACTION_K)
        def action_l(self): self.captured_actions.append(self.AEnum.ACTION_L)
        def action_m(self): self.captured_actions.append(self.AEnum.ACTION_M)
        def action_n(self): self.captured_actions.append(self.AEnum.ACTION_N)
        def action_o(self): self.captured_actions.append(self.AEnum.ACT_O)
        def action_o_else(self): self.captured_actions.append(self.AEnum.ACT_O_EL)
        def action_p(self): self.captured_actions.append(self.AEnum.ACTION_P)
        def action_p_else(self): self.captured_actions.append(self.AEnum.ACTION_P_ELSE)
        
        
    # -- transition tests ---#
    def setUp(self):
        self.actions = self.TActions()
        # self.state_machine = MyStateMachine(self.actions)
        self.state_machine = MyStateMachine(self.actions)
        self.state_machine.init()
        self.states = dict(zip(MyStateMachine.sm_structure.states.keys(), MyStateMachine.sm_structure.states.keys()))
        
    def tearDown(self):
        self.state_machine.exit()
        
    def test_initial_transition_state(self):
#    init:  s0 ->  s11:   s0.entry, s1.entry, s11.entry
        self.state_machine.exit() # make sure the state machine is uninitialized
        v = self.TestVector("init", self.states["s0"], self.state_machine.init, self.states["s11"], [self.TActions.AEnum.ENTERS0, self.TActions.AEnum.ENTERS1, self.TActions.AEnum.ENTERS11])
        self.prove_one_transition(self.state_machine, self.actions, v)

    def test_exit_transition_from_s211(self):
 #    exit:s211 ->   s0: s211.exit, s21.exit, s2.exit, s0.exit
        v = self.TestVector("exit from s211", self.states["s211"], self.state_machine.exit, None, \
            [self.TActions.AEnum.EXITS211, self.TActions.AEnum.EXITS21, self.TActions.AEnum.EXITS2, self.TActions.AEnum.EXITS0])
        self.prove_one_transition(self.state_machine, self.actions, v)

    def test_exit_transition_from_s11(self):
#          s11 ->   s0:  s11.exit, s1.exit, s0.exit
        v = self.TestVector("exit from s11", self.states["s11"], self.state_machine.exit, None, \
            [self.TActions.AEnum.EXITS11, self.TActions.AEnum.EXITS1, self.TActions.AEnum.EXITS0])
        self.prove_one_transition(self.state_machine, self.actions, v)

    def test_s1_to_s1_event_a(self):
#       a:  s1 ->   s1:  s11.exit,  s1.exit,  s1.entry, s11.entry
        v = self.TestVector("s1 to s1 event a", self.states["s11"], self.state_machine.a, self.states["s11"], \
            [self.TActions.AEnum.EXITS11, self.TActions.AEnum.EXITS1, self.TActions.AEnum.TRANS_S1_TO_S1_A ,self.TActions.AEnum.ENTERS1, self.TActions.AEnum.ENTERS11])
        self.prove_one_transition(self.state_machine, self.actions, v)

    def test_s1_to_s11_event_b(self):
#       b:  s1 ->  s11:  s11.exit,  s1.exit,  s1.entry, s11.entry
        v = self.TestVector("s1 to s11 event b", self.states["s11"], self.state_machine.b, self.states["s11"], \
            [self.TActions.AEnum.EXITS11, self.TActions.AEnum.TRANS_S1_TO_S11_B , self.TActions.AEnum.ENTERS11])
        self.prove_one_transition(self.state_machine, self.actions, v)

    def test_s21_to_s211_event_b(self):
#          s21 -> s211: s211.exit, s21.exit, s21.entry, s211.entry
        v = self.TestVector("s21 to s211 event b", self.states["s211"], self.state_machine.b, self.states["s211"], \
            [self.TActions.AEnum.EXITS211, self.TActions.AEnum.TRANS_S21_TO_S211_B, self.TActions.AEnum.ENTERS211])
        self.prove_one_transition(self.state_machine, self.actions, v)

    def test_s1_to_s2_event_c(self):
#       c:  s1 ->   s2:  s11.exit,  s1.exit,  s2.entry, s21.entry, s211.entry
        v = self.TestVector("s1 to s2 event c", self.states["s11"], self.state_machine.c, self.states["s211"], \
            [self.TActions.AEnum.EXITS11, self.TActions.AEnum.EXITS1, self.TActions.AEnum.TRANS_S1_TO_S2_C, self.TActions.AEnum.ENTERS2, self.TActions.AEnum.ENTERS21, self.TActions.AEnum.ENTERS211])
        self.prove_one_transition(self.state_machine, self.actions, v)

    def test_s2_to_s1_event_c(self):
#           s2 ->   s1: s211.exit, s21.exit,  s2.exit, s1.entry, s11.entry
        v = self.TestVector("s2 to s1 event c", self.states["s211"], self.state_machine.c, self.states["s11"], \
            [self.TActions.AEnum.EXITS211, self.TActions.AEnum.EXITS21, self.TActions.AEnum.EXITS2, self.TActions.AEnum.TRANS_S2_TO_S1_C, self.TActions.AEnum.ENTERS1, self.TActions.AEnum.ENTERS11])
        self.prove_one_transition(self.state_machine, self.actions, v)

    def test_s1_to_s0_event_d(self):
#       d:  s1 ->   s0:  s11.exit,  s1.exit,  s1.entry, s11.entry
        v = self.TestVector("s1 to s0 event d", self.states["s11"], self.state_machine.d, self.states["s11"], \
            [self.TActions.AEnum.EXITS11, self.TActions.AEnum.EXITS1, self.TActions.AEnum.TRANS_S1_TO_S0_D, self.TActions.AEnum.ENTERS1, self.TActions.AEnum.ENTERS11])
        self.prove_one_transition(self.state_machine, self.actions, v)

    def test_s211_to_s21_event_d(self):
#         s211 ->  s21: s211.exit, s21.exit, s21.entry, s211.entry
        v = self.TestVector("s211 to s21 event d", self.states["s211"], self.state_machine.d, self.states["s211"], \
            [self.TActions.AEnum.EXITS211, self.TActions.AEnum.TRANS_S211_TO_S21_D, self.TActions.AEnum.ENTERS211])
        self.prove_one_transition(self.state_machine, self.actions, v)
        
    def test_s0_to_s211_event_e_case_s11(self):
#       e:  s0 -> s211:  s11.exit,  s1.exit, s2.entry, s21.entry, s211.entry
        v = self.TestVector("s0 to s211 event e case s11", self.states["s11"], self.state_machine.e, self.states["s211"], \
            [self.TActions.AEnum.EXITS11, self.TActions.AEnum.EXITS1, self.TActions.AEnum.TRANS_S0_TO_S211_E, self.TActions.AEnum.ENTERS2, self.TActions.AEnum.ENTERS21, self.TActions.AEnum.ENTERS211])
        self.prove_one_transition(self.state_machine, self.actions, v)

    def test_s0_t0_s211_event_e_case_s211(self):
#                       s211.exit, s21.exit,  s2.exit, s2.entry, s21.entry, s211.entry
        v = self.TestVector("s0 to s211 event e case s211", self.states["s211"], self.state_machine.e, self.states["s211"], \
            [self.TActions.AEnum.EXITS211, self.TActions.AEnum.EXITS21, self.TActions.AEnum.EXITS2, self.TActions.AEnum.TRANS_S0_TO_S211_E, self.TActions.AEnum.ENTERS2, self.TActions.AEnum.ENTERS21, self.TActions.AEnum.ENTERS211])
        self.prove_one_transition(self.state_machine, self.actions, v)

    def test_s2_to_s11_event_f(self):
#       f:  s2 ->  s11: s211.exit, s21.exit,  s2.exit, s1.entry, s11.entry
        v = self.TestVector("s2 to s11 event f", self.states["s211"], self.state_machine.f, self.states["s11"], \
            [self.TActions.AEnum.EXITS211, self.TActions.AEnum.EXITS21, self.TActions.AEnum.EXITS2, self.TActions.AEnum.TRANS_S2_TO_S11_F, self.TActions.AEnum.ENTERS1, self.TActions.AEnum.ENTERS11])
        self.prove_one_transition(self.state_machine, self.actions, v)

    def test_s1_to_s211_event_f(self):
#           s1 -> s211:  s11.exit,  s1.exit,  s2.entry, s21.entry, s211.entry
        v = self.TestVector("s1 to s211 event f", self.states["s11"], self.state_machine.f, self.states["s211"], \
            [self.TActions.AEnum.EXITS11, self.TActions.AEnum.EXITS1, self.TActions.AEnum.TRANS_S1_TO_S211_F, self.TActions.AEnum.ENTERS2, self.TActions.AEnum.ENTERS21, self.TActions.AEnum.ENTERS211])
        self.prove_one_transition(self.state_machine, self.actions, v)

    def test_s11_to_s211_event_g(self):
#       g: s11 -> s211:  s11.exit,  s1.exit,  s2.entry, s21.entry, s211.entry
        v = self.TestVector("s11 to s211 event g", self.states["s11"], self.state_machine.g, self.states["s211"], \
            [self.TActions.AEnum.EXITS11, self.TActions.AEnum.EXITS1, self.TActions.AEnum.TRANS_S11_TO_S211_G, self.TActions.AEnum.ENTERS2, self.TActions.AEnum.ENTERS21, self.TActions.AEnum.ENTERS211])
        self.prove_one_transition(self.state_machine, self.actions, v)

    def test_s211_to_s0_event_g(self):
#         s211 ->   s0: s211.exit, s21.exit,  s2.exit,  s1.entry, s11.entry
        v = self.TestVector("s211 to s0 event g", self.states["s211"], self.state_machine.g, self.states["s11"], \
            [self.TActions.AEnum.EXITS211, self.TActions.AEnum.EXITS21, self.TActions.AEnum.EXITS2, self.TActions.AEnum.TRANS_S211_TO_S0_G, self.TActions.AEnum.ENTERS1, self.TActions.AEnum.ENTERS11])
        self.prove_one_transition(self.state_machine, self.actions, v)

    def test_s11_to_s11_event_h_guard_true(self):
#       h: s11 foo==True: actions.check_foo, actions.unset_foo
        self.actions.foo = True
        v = self.TestVector("test_s11_to_s11_event_h_guard_true", self.states["s11"], self.state_machine.h, self.states["s11"], \
            [self.TActions.AEnum.CHECKFOO, self.TActions.AEnum.TRANS_S11_TO_S11_H, self.TActions.AEnum.UNSETFOO])
        self.prove_one_transition(self.state_machine, self.actions, v)

    def test_s11_to_s11_event_h_guard_false(self):
#       h: s11 foo==False: actions.check_foo
        self.actions.foo = False
        v = self.TestVector("test_s11_to_s11_event_h_guard_false", self.states["s11"], self.state_machine.h, self.states["s11"], \
            [self.TActions.AEnum.CHECKFOO])
        self.prove_one_transition(self.state_machine, self.actions, v)

    def test_s211_to_s211_event_h_guard_false(self):
#          s21 foo==False: s211.exit, s21.exit, actions.set_foo, s21.entry, s211.entry
        self.actions.foo = False
        v = self.TestVector("test_s211_to_s211_event_h_guard_false", self.states["s211"], self.state_machine.h, self.states["s211"], \
            [self.TActions.AEnum.CHECKFOO, self.TActions.AEnum.EXITS211, self.TActions.AEnum.EXITS21, self.TActions.AEnum.TRANS_S21_TO_S21_H, self.TActions.AEnum.SETFOO, self.TActions.AEnum.ENTERS21, self.TActions.AEnum.ENTERS211])
        self.prove_one_transition(self.state_machine, self.actions, v)

    def test_s211_to_s211_event_h_guard_true(self):
#          s21 foo==False: s211.exit, s21.exit, actions.set_foo, s21.entry, s211.entry
        self.actions.foo = True
        v = self.TestVector("test_s211_to_s211_event_h_guard_true", self.states["s211"], self.state_machine.h, self.states["s211"], \
            [self.TActions.AEnum.CHECKFOO])
        self.prove_one_transition(self.state_machine, self.actions, v)

    def test_s211_to_s211_event_a_no_guard(self):
#          s21 foo==False: s211.exit, s21.exit, actions.set_foo, s21.entry, s211.entry
        v = self.TestVector("test_s211_to_s211_event_a_no_guard", self.states["s211"], self.state_machine.a, self.states["s211"], \
            [])
        self.prove_one_transition(self.state_machine, self.actions, v)
        
    def test_s11_to_s11_event_i(self):
        v = self.TestVector("test_s11_to_s11_event_i", self.states["s11"], self.state_machine.i, self.states["s11"], \
            [self.TActions.AEnum.ACTION_I])
        self.prove_one_transition(self.state_machine, self.actions, v)

    def test_s211_to_s211_event_i(self):
        v = self.TestVector("test_s211_to_s211_event_i", self.states["s211"], self.state_machine.i, self.states["s211"], \
            [self.TActions.AEnum.ACTION_I])
        self.prove_one_transition(self.state_machine, self.actions, v)

    def test_s11_to_s11_event_j(self):
        v = self.TestVector("test_s11_to_s11_event_j", self.states["s11"], self.state_machine.j, self.states["s11"], \
            [self.TActions.AEnum.ACTION_J])
        self.prove_one_transition(self.state_machine, self.actions, v)

    def test_s211_to_s211_event_j(self):
        v = self.TestVector("test_s211_to_s211_event_j", self.states["s211"], self.state_machine.j, self.states["s211"], [])
        self.prove_one_transition(self.state_machine, self.actions, v)
        
    def test_s211_to_s211_event_k(self):
        v = self.TestVector("test_s211_to_s211_event_k", self.states["s211"], self.state_machine.k, self.states["s211"], \
            [self.TActions.AEnum.ACTION_K])
        self.prove_one_transition(self.state_machine, self.actions, v)

    def test_s11_to_s11_event_k(self):
        v = self.TestVector("test_s11_to_s11_event_k", self.states["s11"], self.state_machine.k, self.states["s11"], \
            [])
        self.prove_one_transition(self.state_machine, self.actions, v)

    def test_s21_to_s21_event_l_guard_True(self):
        self.actions.foo = False
        v = self.TestVector("test_s21_to_s21_event_l", self.states["s211"], self.state_machine.l, self.states["s211"], \
            [self.TActions.AEnum.CHECKFOO])
        self.prove_one_transition(self.state_machine, self.actions, v)
        
    def test_s21_to_s21_event_l_guard_False(self):
        self.actions.foo = True
        v = self.TestVector("test_s21_to_s21_event_l", self.states["s211"], self.state_machine.l, self.states["s211"], \
            [self.TActions.AEnum.CHECKFOO, self.TActions.AEnum.ACTION_L])
        self.prove_one_transition(self.state_machine, self.actions, v)

    def test_s11_to_s11_event_l(self):
        v = self.TestVector("test_s11_to_s11_event_l", self.states["s11"], self.state_machine.l, self.states["s11"], \
            [])
        self.prove_one_transition(self.state_machine, self.actions, v)

    def test_s211_to_s211_event_m(self):
        v = self.TestVector("test_s211_to_s211_event_m", self.states["s211"], self.state_machine.m, self.states["s211"], \
            [self.TActions.AEnum.ACTION_M])
        self.prove_one_transition(self.state_machine, self.actions, v)

    def test_s11_to_s11_event_m(self):
        v = self.TestVector("test_s11_to_s11_event_m", self.states["s11"], self.state_machine.m, self.states["s11"], \
            [])
        self.prove_one_transition(self.state_machine, self.actions, v)

    def test_s11_to_s11_event_n(self):
        v = self.TestVector("test_s11_to_s11_event_n", self.states["s11"], self.state_machine.n, self.states["s11"], \
            [self.TActions.AEnum.ACTION_N])
        self.prove_one_transition(self.state_machine, self.actions, v)
        
    def test_s211_to_s211_event_n(self):
        v = self.TestVector("test_s211_to_s211_event_n", self.states["s211"], self.state_machine.n, self.states["s211"], \
            [])
        self.prove_one_transition(self.state_machine, self.actions, v)

    def test_s11_to_s11_event_o_guard_True(self):
#           o:     s11 ->  s11:     foo=True: s11.exit, act_o, s11.entry                                  external transition
        self.actions.foo = True
        v = self.TestVector("test_s11_to_s11_event_o_guard_True", self.states["s11"], self.state_machine.o, self.states["s11"], \
            [self.TActions.AEnum.CHECKFOO, self.TActions.AEnum.EXITS11, self.TActions.AEnum.ACT_O, self.TActions.AEnum.ENTERS11])
        self.prove_one_transition(self.state_machine, self.actions, v)
        
    def test_s11_to_s211_event_o_guard_False_else_transition(self):
#           o:     s11 ->   s2:     foo=False: s11.exit, s1.exit, act_o_el, s2.entry, s21.entry, s211.entry  external transition (else path of guard)
        self.actions.foo = False
        v = self.TestVector("test_s11_to_s211_event_o_guard_False", self.states["s11"], self.state_machine.o, self.states["s211"], \
            [self.TActions.AEnum.CHECKFOO, self.TActions.AEnum.EXITS11, self.TActions.AEnum.EXITS1, self.TActions.AEnum.ACT_O_EL, self.TActions.AEnum.ENTERS2, self.TActions.AEnum.ENTERS21, self.TActions.AEnum.ENTERS211])
        self.prove_one_transition(self.state_machine, self.actions, v)
        
    def test_s211_to_s211_event_o_guard_True(self):
#           o:    s211 -> s211:     foo=True: act_o                                                       internal transition
        self.actions.foo = True
        v = self.TestVector("test_s211_to_s211_event_o_guard_True", self.states["s211"], self.state_machine.o, self.states["s211"], \
            [self.TActions.AEnum.CHECKFOO, self.TActions.AEnum.ACT_O])
        self.prove_one_transition(self.state_machine, self.actions, v)
        
    def test_s211_to_s211_event_o_guard_False_else_transition(self):
#           o:    s211 -> s211:     foo=False: s211.exit, act_o_el, s211.entry                            local transition (else path of guard)
        self.actions.foo = False
        v = self.TestVector("test_s211_to_s211_event_o_guard_False_else_transition", self.states["s211"], self.state_machine.o, self.states["s211"], \
            [self.TActions.AEnum.CHECKFOO, self.TActions.AEnum.EXITS211, self.TActions.AEnum.ACT_O_EL, self.TActions.AEnum.ENTERS211])
        self.prove_one_transition(self.state_machine, self.actions, v)
        
    def test_s211_to_s211_event_p_guard_True_transition(self):
#           p:    s211 -> s211:     foo=True: s211.exit, s21.exit, action_p, s21.entry, s211.entry        local transition
        self.actions.foo = True
        v = self.TestVector("test_s211_to_s211_event_p_guard_True_transition", self.states["s211"], self.state_machine.p, self.states["s211"], \
            [self.TActions.AEnum.CHECKFOO, self.TActions.AEnum.EXITS211, self.TActions.AEnum.EXITS21, self.TActions.AEnum.ACTION_P, self.TActions.AEnum.ENTERS21, self.TActions.AEnum.ENTERS211])
        self.prove_one_transition(self.state_machine, self.actions, v)
        
    def test_s211_to_s211_event_p_guard_False_else_transition(self):
#           p:    s211 -> s211:     foo=False: s211.exit, s21.exit, s2.exit, action_p_else, s2.entry, s21.entry, s211.entry     external transition (else path of guard)
        self.actions.foo = False
        v = self.TestVector("test_s211_to_s211_event_p_guard_False_else_transition", self.states["s211"], self.state_machine.p, self.states["s211"], \
            [self.TActions.AEnum.CHECKFOO, self.TActions.AEnum.EXITS211, self.TActions.AEnum.EXITS21, self.TActions.AEnum.EXITS2, self.TActions.AEnum.ACTION_P_ELSE, self.TActions.AEnum.ENTERS2, self.TActions.AEnum.ENTERS21, self.TActions.AEnum.ENTERS211])
        self.prove_one_transition(self.state_machine, self.actions, v)


        
    # def test_sequence_AAA(self):
        # vecs  = [
            # # self.TestVector("s1 to s211 event f", self.states["s11"], self.state_machine.f, self.states["s211"], \
                # # [self.TActions.AEnum.EXITS11, self.TActions.AEnum.EXITS1, self.TActions.AEnum.TRANS_S1_TO_S211_F, self.TActions.AEnum.ENTERS2, self.TActions.AEnum.ENTERS21, self.TActions.AEnum.ENTERS211]),
            # # self.TestVector("s2 to s1 event c", self.states["s211"], self.state_machine.c, self.states["s11"], \
                # # [self.TActions.AEnum.EXITS211, self.TActions.AEnum.EXITS21, self.TActions.AEnum.EXITS2, self.TActions.AEnum.TRANS_S2_TO_S1_C, self.TActions.AEnum.ENTERS1, self.TActions.AEnum.ENTERS11]),
            # self.TestVector("test_s11_to_s11_event_h_guard_false", self.states["s11"], self.state_machine.h, self.states["s11"], \
                # [self.TActions.AEnum.CHECKFOO]),
            # self.TestVector("s1 to s2 event c", self.states["s11"], self.state_machine.c, self.states["s211"], \
                # [self.TActions.AEnum.EXITS11, self.TActions.AEnum.EXITS1, self.TActions.AEnum.TRANS_S1_TO_S2_C, self.TActions.AEnum.ENTERS2, self.TActions.AEnum.ENTERS21, self.TActions.AEnum.ENTERS211]),
            # ]
        
        # self.prove_transition_sequence(self.state_machine, self.actions, vecs)
    
    
# ------------------------------------------------------------------------------

class SympleHFSMTestAllFeaturesOptimized(SympleHFSMTestAllFeatures):

    def setUp(self):
        MyStateMachine.sm_structure.do_optimize()
        SympleHFSMTestAllFeatures.setUp(self)

