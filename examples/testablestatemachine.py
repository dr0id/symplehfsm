#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
.. TODO:: write test according to article!!

This is an example following an article of the internet showing how to make a state machine testable. The
framework supports testing of state machines. 


This is an example state machine following this article:

http://accu.org/index.php/journals/1548

::

            +----------------------------------------------------------------------------------+
            |                                                                                  |
    init -->|                                                                                  |
            |       +-------+  dark/   +---------------------------------------------------+   |
            | *---->| day   |--------->| night                                             |   |
            |   +-->|       |          |                                                   |   |
            |   |   +-------+          |      initial    +--------------+                  |   |
            |   |                      |    *----------->| off          |--+               |   |
            |   |                      |           +---->|              |  |               |   |
            |   |                      |           |     +--------------+  | movement/     |   |
            |   +----------------------|           |                       |  lamp_on      |   |
            |       light/lamp_off     | timeout/  |     +--------------+  |               |   |
            |                          |  lamp_off |     | moving       |<-+               |   |
            |                          |           |  +->|              |                  |   |
            |                          |           |  |  |              |--+               |   |
            |                          |           |  |  +--------------+  |               |   |
            |                          |           |  |                    |               |   |
            |                          |           |  | movement/          |               |   |
            |                          |           |  |                    | no_movement/  |   |
            |                          |           |  |  +--------------+  |   start_timer |   |
            |                          |           |  |  | timing       |  |               |   |
            |                          |           |  +--|              |<-+               |   |
            |                          |           |     |              |                  |   |
            |                          |           +-----|              |                  |   |
            |                          |                 |              |                  |   |
            |                          |                 |              |                  |   |
            |                          |                 +--------------+                  |   |
            |                          |                                                   |   |
            |                          +---------------------------------------------------+   |
            |                                                                                  |
            +----------------------------------------------------------------------------------+



"""

__version__ = "1.0.3.0"
__author__ = "dr0iddr0id {at} gmail [dot] com (C) 2010"

from operator import methodcaller

import symplehfsm
from symplehfsm import Structure
from symplehfsm import SympleHFSM
from symplehfsm import BaseHFSMTests

# ------------------------------------------------------------------------------


# ------------------------------------------------------------------------------
class Events(object):
    dark = 0
    light = 1
    movement = 2
    no_movement = 3
    timeout = 4

# ------------------------------------------------------------------------------

class Actions(object):
    def lamp_off(self):
        print("-> action: lamp off")
    def lamp_on(self):
        print("-> action: lamp on")
    def start_timer(self):
        print("-> action: starting timer")

# ------------------------------------------------------------------------------

def main():
    structure = Structure()

    #                         state,    parent, initial,            entry,        exit
    structure.add_state("parent",        None, False)
    structure.add_state(  "day",     "parent",  True)
    structure.add_state(  "night",   "parent", False)
    structure.add_state(    "off",    "night",  True)
    structure.add_state(    "moving", "night", False)
    structure.add_state(    "timing", "night", False)

    #                handling state,   event, next state,           action,                    guard
    structure.add_trans(   "day",        "dark",  "night")
    structure.add_trans( "night",       "light",    "day", methodcaller("lamp_off"))
    structure.add_trans(   "off",    "movement", "moving", methodcaller("lamp_on"))
    structure.add_trans("moving", "no_movement", "timing", methodcaller("start_timer"))
    structure.add_trans("timing",     "timeout",    "off", methodcaller("lamp_off"))
    structure.add_trans("timing",    "movement", "moving")

    actions = Actions()

    sm = SympleHFSM(structure, actions)
    sm.init(False)


    # run a event sequence
    print("event: dark")
    sm.handle_event("dark")
    print("event: movement")
    sm.handle_event("movement")
    print("event: no_movement")
    sm.handle_event("no_movement")
    print("event: movement")
    sm.handle_event("movement")
    print("event: no_movement")
    sm.handle_event("no_movement")
    print("event: timeout")
    sm.handle_event("timeout")
    print("event: light")
    sm.handle_event("light")
    print("event: dark")
    sm.handle_event("dark")

if __name__ == "__main__":
    main()

# ------------------------------------------------------------------------------
