This are the examples for the symplehfsm project. Make sure that the symplehfsm module can be imported before running them.


mousefollower:
    This example uses pygame (see: www.pygame.org). This example has a detailed introduction how to use SympleHFSM. See
    the doc-string of the module.
    
testablestatemachines:
    This is an example following an article of the internet showing how to make a state machine testable. The
    framework supports testing of state machines. 
    
    
TestHFSM:
    This is a full blown example. It uses all features of the symplehfsm framework (entry-, exit- and 
    transition-actions, guards). There is at least one transition for each type (external transition, 
    local transition, internal transition). There are also tests for each transition. This example shows 
    the usage of different actions implementations applied to the same state machine (one for testing, 
    one for the interactive demo).


