Symplehfsm
==========
A python framework for easy writing hierarchical finite state machines. 


Homepage
--------

http://dr0id.bitbucket.org/python/symplehfsm/


Source
------

Get the source on bitbucket

https://bitbucket.org/dr0id/symplehfsm/


Installing
----------

Download the source and run

```sh
$ python setup.py install
```


Examples
--------

See the examples folder for usage.


Copyright (c) 2012 - 2016, DR0ID
-------------------------
All rights reserved.

All code is under following license (for details see LICENSE.txt):

New BSD license 







